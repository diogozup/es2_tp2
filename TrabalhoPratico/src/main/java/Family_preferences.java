public class Family_preferences {

    //vars
    int family_id;
    int choice[];
    int n_people;
    boolean isServed;



    public int getFamily_id() {
        return family_id;
    }

    public void setFamily_id(int family_id) {
        this.family_id = family_id;
    }

    public int[] getChoice() {
        return choice;
    }

    public void setChoice(int[] choice) {
        this.choice = choice;
    }

    public int getN_people() {
        return n_people;
    }

    public void setN_people(int n_people) {
        this.n_people = n_people;
    }

    public boolean isIsServed() {
        return isServed;
    }

    public void setIsServed(boolean isServed) {
        this.isServed = isServed;
    }











    public Family_preferences(int family_id, int[] choice, int n_people, boolean isServed) {
        this.family_id = family_id;
        this.choice = choice;
        this.n_people = n_people;
        this.isServed = isServed;
    }






    public String stringchoice() {
        String s = "";
        for (int j = 0; j < choice.length; j++) {
            s+= " " + choice[j];
        }
        return s;
    }




    public String toString() {
        return "family_preferences{" + "family_id=" + family_id + ", choice=" + stringchoice() + ", n_people=" + n_people + " ,servida: "+ isServed +'}';
    }

}
